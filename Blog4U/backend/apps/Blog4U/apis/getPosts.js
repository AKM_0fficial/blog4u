/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: MIT - see enclosed LICENSE file.
 */

// Custom modules
const API_CONSTANTS = require(`${CONSTANTS.APPROOTDIR}/Blog4U/apis/lib/constants`);
const db = require(`${CONSTANTS.APPROOTDIR}/Blog4U/apis/lib/db`);

exports.doService = async jsonReq => {
    // Validate API request and check mandatory payload required
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

    try {
        const posts = await getPosts(jsonReq);
        if (!posts) return API_CONSTANTS.API_RESPONSE_FALSE;
        return { result: true, results: { posts } };
    } catch (error) {
        console.error(error);
        return API_CONSTANTS.API_RESPONSE_SERVER_ERROR;
    }
}

const getPosts = async (jsonReq) => {
    try {
        const posts = await db.simpleSelect('SELECT * FROM blog;');
        if(!posts) return false;
        return posts;
    } catch (error) {
        throw error;
    }
}

const validateRequest = jsonReq => (jsonReq);
