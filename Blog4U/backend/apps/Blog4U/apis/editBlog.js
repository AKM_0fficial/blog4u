/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: MIT - see enclosed LICENSE file.
 */

// Custom modules
const API_CONSTANTS = require(`${CONSTANTS.APPROOTDIR}/Blog4U/apis/lib/constants`);
const db = require(`${CONSTANTS.APPROOTDIR}/Blog4U/apis/lib/db`);

exports.doService = async jsonReq => {
    // Validate API request and check mandatory payload required
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

    try {
        const edittedPost = await editBlog(jsonReq);
        if (!edittedPost) return API_CONSTANTS.API_RESPONSE_FALSE;
        return { result: true, results: { edittedPost } };
    } catch (error) {
        console.error(error);
        return API_CONSTANTS.API_RESPONSE_SERVER_ERROR;
    }
}

const editBlog = async (jsonReq) => {
    try {
        const queryParams = [jsonReq.name,jsonReq.author,jsonReq.content,jsonReq.id];
        console.log(queryParams);
        if (!(await db.simpleUpdate("UPDATE blog SET name=?, author=?, content=? WHERE id=?",queryParams )))
          return false;
        return true;
    } catch (error) {
        throw error;
    }
}

const validateRequest = jsonReq => (jsonReq) ;
