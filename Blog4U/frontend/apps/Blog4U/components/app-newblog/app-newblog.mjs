/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
import { router } from "/framework/js/router.mjs";
import { monkshu_component } from "/framework/js/monkshu_component.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

const createNewblog = async () => {
	let jsonres={
		"name":app_newblog.shadowRoot.querySelector("#name").value,
		"author":app_newblog.shadowRoot.querySelector("#author").value,
		"content":app_newblog.shadowRoot.querySelector("#content").value
	};
    let resp = await apiman.rest(APP_CONSTANTS.API_CREATE, "POST", jsonres, true);
	
	if (!resp.result) router.reload();
	router.loadPage(APP_CONSTANTS.POSTS_HTML);
}

function register() {
    // convert this all into a WebComponent so we can use it
    monkshu_component.register("app-newblog", `${APP_CONSTANTS.APP_PATH}/components/app-newblog/app-newblog.html`, app_newblog);
}

function gotopage(page){
    console.log(page);
	router.loadPage(page);
}
const trueWebComponentMode = true;	// making this false renders the component without using Shadow DOM

export const app_newblog = { trueWebComponentMode, register, createNewblog, gotopage }