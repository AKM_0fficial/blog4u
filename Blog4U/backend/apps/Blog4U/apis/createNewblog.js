/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: MIT - see enclosed LICENSE file.
 */

// Custom modules
const API_CONSTANTS = require(`${CONSTANTS.APPROOTDIR}/Blog4U/apis/lib/constants`);
const db = require(`${CONSTANTS.APPROOTDIR}/Blog4U/apis/lib/db`);

exports.doService = async jsonReq => {
    // Validate API request and check mandatory payload required
    if (!validateRequest(jsonReq)) return API_CONSTANTS.API_INSUFFICIENT_PARAMS;

    try {
        const createdPost = await createNewblog(jsonReq);
        if (!createdPost) return API_CONSTANTS.API_RESPONSE_FALSE;
        return { result: true, results: { createdPost } };
    } catch (error) {
        console.error(error);
        return API_CONSTANTS.API_RESPONSE_SERVER_ERROR;
    }
}

const createNewblog = async (jsonReq) => {
    try {
        const queryParams = {"name":jsonReq.name,"author":jsonReq.author,"content":jsonReq.content};
        if (!(await db.simpleInsert("blog",queryParams )))
          return false;
        return true;
    } catch (error) {
        throw error;
    }
}

const validateRequest = jsonReq => (jsonReq && jsonReq.name && jsonReq.author && jsonReq.content) ;
