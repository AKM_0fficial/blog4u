/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
import { router } from "/framework/js/router.mjs";
import { monkshu_component } from "/framework/js/monkshu_component.mjs";
import { apimanager as apiman } from "/framework/js/apimanager.mjs";

let resp,flag=true,i=0,id;

const getPosts = async () => {

    resp = await apiman.rest(APP_CONSTANTS.API_POSTS, "POST", {}, false, true);
    
    if (!resp || !resp.result) router.reload();

    if(app_posts.shadowRoot.querySelector("#get-posts").value == "Previous"){
        i--;
        if(i<0) i=resp.results.posts.length-1;
    }
    else if(app_posts.shadowRoot.querySelector("#get-posts").value == "Next"){
        i++;
        if(i>resp.results.posts.length) i=0;
    }
    else{
        i=resp.results.posts.length-1;
    } 
    id=resp.results.posts[i].id;
    app_posts.shadowRoot.querySelector("#name").value = resp.results.posts[i].name;
    app_posts.shadowRoot.querySelector("#author").value = resp.results.posts[i].author;
    app_posts.shadowRoot.querySelector("#content").value = resp.results.posts[i].content;
    return;
}


const editPost = async () => {

    console.log(app_posts.shadowRoot.querySelector("#get-posts").value);  
     
    let jsonres={
        "id":id,
        "name":app_posts.shadowRoot.querySelector("#name").value,
        "author":app_posts.shadowRoot.querySelector("#author").value,
        "content":app_posts.shadowRoot.querySelector("#content").value
    };
     
    let resp = await apiman.rest(APP_CONSTANTS.API_EDIT, "POST", jsonres, true);
    
    if (!resp.result) router.reload();

    router.loadPage(APP_CONSTANTS.POSTS_HTML);
    
}

function register() {
    // convert this all into a WebComponent so we can use it
    monkshu_component.register("app-posts", `${APP_CONSTANTS.APP_PATH}/components/app-posts/app-posts.html`, app_posts);
    getPosts();
}

function gotopage(page){
    console.log(page);
	router.loadPage(page);
}
const trueWebComponentMode = true;	// making this false renders the component without using Shadow DOM

export const app_posts = { trueWebComponentMode, register, getPosts, gotopage, editPost }