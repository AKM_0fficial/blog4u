/* 
 * (C) 2020 TekMonks. All rights reserved.
 * License: MIT - see enclosed license.txt file.
 */
const FRONTEND = "http://localhost:7070";
const BACKEND = "http://localhost:9090";
const APP_NAME = "Blog4U";
const APP_PATH = `${FRONTEND}/apps/${APP_NAME}`;

export const APP_CONSTANTS = {
    FRONTEND, BACKEND, APP_PATH, APP_NAME,
    POSTS_HTML: APP_PATH + "/posts.html",
    CREATE_HTML: APP_PATH + "/newblog.html",
    ABOUT_HTML: APP_PATH + "/aboutus.html",

    SESSION_NOTE_ID: "com_monkshu_ts",

    API_POSTS: `${BACKEND}/apis/get-posts`,
    API_CREATE: `${BACKEND}/apis/new-blog`,
    API_EDIT: `${BACKEND}/apis/edit-blog`,

    USERID: "id",
    USER_ROLE: "user",
    GUEST_ROLE: "guest",
    PERMISSIONS_MAP: {
        user: [APP_PATH + "/posts.html", $$.MONKSHU_CONSTANTS.ERROR_THTML],
        guest: [APP_PATH + "/newblog.html", APP_PATH + "/aboutus.html", APP_PATH + "/posts.html", $$.MONKSHU_CONSTANTS.ERROR_THTML]
    },
    API_KEYS: { "*": "uiTmv5YBOZMqdTb0gekD40PnoxtB9Q0k" },
    KEY_HEADER: "X-API-Key"
}